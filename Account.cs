﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing3
{
    public class Account
    {
        private static int _numberOfAcc;
        private readonly int _accountNumber;
        private readonly Customer _accountOwner;
        private int _maxMinusAllowed;

        public Account(Customer accountOwner, int monthlyIncome)
        {
            _maxMinusAllowed = -(monthlyIncome * 3);
            _accountOwner = accountOwner;
            _accountNumber = ++_numberOfAcc;
        }

        public int AccountNumber { get; }
        public int Balance { get; private set; }
        public Customer AccountOwner { get { return _accountOwner; } }
        public int MaxMinusAllowed { get { return _maxMinusAllowed; } }
        public override bool Equals(object obj)
        {
            return this == obj as Account;
        }
        public void Add (int amount)
        {
            Balance = Balance + amount;
        }
        public void Subtract(int amount)
        {
            if (Balance - amount < _maxMinusAllowed)
                throw new BalanceException($"you cant redraw {amount} because it overstep the maximum minus allowed which is {_maxMinusAllowed}");
            else
            Balance -= amount;
        }

        public override int GetHashCode()
        {
            return _accountNumber;
        }

        public override string ToString()
        {
            return $"account number: {AccountNumber}, account owner: {AccountOwner}, balance: {Balance}, maximum minimum allowed: {MaxMinusAllowed}";
        }

        public static bool operator ==(Account a1, Account a2)
        {
            if (a1 is null && a2 is null)
                return true;
            if (a1 is null || a2 is null)
                return false;
            return a1._accountNumber == a2._accountNumber;
        }
        public static bool operator !=(Account a1, Account a2)
        {
            if (a1 is null && a2 is null)
                return false;
            if (a1 is null || a2 is null)
                return true;
            return a1._accountNumber != a2._accountNumber;
        }
        public static Account operator +(Account a1, Account a2)
        {
            if (a1 == a2)
            {
                Account newAccount = new Account(a1._accountOwner, a1.MaxMinusAllowed / 3);
                newAccount.Add(a1.Balance + a2.Balance);
                return newAccount;
            }
            throw new NotSameCustomerException("you tried to joing accounts of different customers");
        }
        public static int operator +(Account a, int amount)
        {
            a.Balance += amount;
            return a.Balance;
        }
        public static int operator -(Account a, int amount)
        {
            if ((a.Balance - amount) >= a.MaxMinusAllowed)
            {
                a.Balance -= amount;
                return a.Balance;
            }
            throw new BalanceException($"that amount {amount} will overstep the maximum minus allowed on this account");
        }
    }
}
