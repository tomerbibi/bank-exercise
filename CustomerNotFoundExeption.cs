﻿using System;
using System.Runtime.Serialization;

namespace ClassThing3
{
    [Serializable]
    internal class CustomerNotFoundExeption : Exception
    {
        public CustomerNotFoundExeption()
        {
        }

        public CustomerNotFoundExeption(string message) : base(message)
        {
        }

        public CustomerNotFoundExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CustomerNotFoundExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}