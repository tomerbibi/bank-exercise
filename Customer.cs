﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing3
{
    public class Customer
    {
        private static int _numberOfCust = 0;
        private readonly int _customerID;
        private readonly int _CustomerNumber;

        public Customer(int customerID, string name, int phNumber)
        {
            _customerID = customerID;
            Name = name;
            PhNumber = phNumber;
            _CustomerNumber = ++_numberOfCust;

        }

        public string Name { get; private set; }
        public int PhNumber { get; private set; }
        public int CustomerId { get{return _customerID;}}
        public int CustomerNumber { get { return _customerID; } }

        public override bool Equals(object obj)
        {
            return this == obj as Customer;
        }

        public override int GetHashCode()
        {
            return CustomerId;
        }

        public override string ToString()
        {
            return $"name: {Name}, phone number: {PhNumber}, id: {CustomerId}, customer number: {CustomerNumber}";
        }

        public static bool operator == (Customer c1, Customer c2)
        {
            if (c1 is null && c2 is null)
                return true;
            if (c1 is null || c2 is null)
                return false;
            return c1.CustomerNumber == c2.CustomerNumber;
        }
        public static bool operator !=(Customer c1, Customer c2)
        {
            if (c1 is null && c2 is null)
                return false;
            if (c1 is null || c2 is null)
                return true;
            return c1.CustomerNumber != c2.CustomerNumber;
        }


    }
}
