﻿using System;
using System.Runtime.Serialization;

namespace ClassThing3
{
    [Serializable]
    internal class AccountAlreadyExistExeption : Exception
    {
        public AccountAlreadyExistExeption()
        {
        }

        public AccountAlreadyExistExeption(string message) : base(message)
        {
        }

        public AccountAlreadyExistExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AccountAlreadyExistExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}