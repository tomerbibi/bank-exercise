﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing3
{
    public interface IBank
    {
        string Name { get; }
        string Adress { get; }
        int CustomerCount { get; }
    }
}
