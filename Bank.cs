using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassThing3
{
    public class Bank: IBank
    {
        private List<Account> _accounts = new List<Account>();
        private List<Customer> _customers = new List<Customer>();
        private Dictionary<int, Customer> _mapCustomerById = new Dictionary<int, Customer>();
        private Dictionary<int, Customer> _mapCustomerByCustomerNumber = new Dictionary<int, Customer>();
        private Dictionary<int, Account> _mapAccountByAccountNmber = new Dictionary<int, Account>();
        private Dictionary<Customer, List<Account>> _mapAccountsByCustomer = new Dictionary<Customer, List<Account>>();
        private int _totalMoneyInBank = 0;
        private float _profits = 0; // i made it float for the ChargeAnnualCommission fnction.

        public Bank()
        {
        }

        public string Name { get { return "israel bank"; } }

        public string Adress { get { return "jerusalem 8"; } }

        public int CustomerCount { get { return _accounts.Count; } }
        internal void AddNewCustomer(Customer c)
        {
            if (_customers.Contains(c))
                throw new CustomerAlreadyExistExeption($"the customer {c} already exist");
            _customers.Add(c);
            _mapCustomerById.Add(c.CustomerId, c);
            _mapCustomerByCustomerNumber.Add(c.CustomerNumber, c);
        }
        internal void OpenNewAccount(Account account, Customer customer)
        {
            if (_accounts.Contains(account))
                throw new AccountAlreadyExistExeption($"the account {account} already exist");
            _accounts.Add(account);
            _mapAccountByAccountNmber.Add(account.AccountNumber, account);
            List<Account> l = new List<Account>();
            _accounts.ForEach(item =>
            {
                if (item.AccountOwner == customer)
                    l.Add(item);
            }
            );
            _mapAccountsByCustomer.Add(customer, l);
        }
        internal Customer GetCustomerById(int id)
        {
            if(_mapCustomerById.ContainsKey(id))
            return _mapCustomerById[id];
            throw new CustomerNotFoundExeption($"the customer with the id {id} was not found");
        }
        internal Customer GetCustomerByNumber(int customerNumber)
        {
            if (_mapCustomerByCustomerNumber.ContainsKey(customerNumber))
                return _mapCustomerByCustomerNumber[customerNumber];
            throw new CustomerNotFoundExeption($"the customer with the customer number {customerNumber} was not found");
        }
        internal List<Account> GetAccountsByCustomer(Customer c)
        {
            if (_customers.Contains(c))
            {
                List<Account> l = new List<Account>();
                _accounts.ForEach(item =>
                {
                    if (item.AccountOwner == c)
                        l.Add(item);
                }
                );
                return l;
            }
            throw new CustomerNotFoundExeption($"the customer {c} was not found");
        }
        internal int Deposit (Account account, int amount)
        {
            if (_accounts.Contains(account))
            {
                account.Add(amount);
                _totalMoneyInBank += amount;
            }
            throw new AccountNotFoundExeption($"the account {account} was not found");
        }
        internal int Withdraw(Account account, int amount)
        {
            if (_accounts.Contains(account) && (account.Balance - amount) > account.MaxMinusAllowed)
            {
                account.Subtract(amount);
                _totalMoneyInBank -= amount;
            }
            throw new AccountNotFoundExeption($"the account {account} was not found");
        }
        internal int GetCustomerTotalBalance(Customer c)
        {
            // you wrote that this function needs to get an account but we need to get a customer to get the balance from ALL of his accounts.
            if (_customers.Contains(c))
            {
                int totalBalance = 0;
                _accounts.ForEach(item =>
                {
                    if (item.AccountOwner == c)
                        totalBalance += item.Balance;
                }
                );
                return totalBalance;
            }
            throw new CustomerNotFoundExeption($"the customer {c} was not found");
        }
        internal void CloseAccount(Account account, Customer customer)
        {
            if (_accounts.Contains(account))
            {
                _accounts.Remove(account);
                _mapAccountByAccountNmber.Remove(account.AccountNumber);
            }
            throw new AccountNotFoundExeption($"the account {account} was not found");
        }
        public void ChargeAnnualCommission (float precentage)
        {
            float addToProfit = 0;
            _accounts.ForEach(item =>
            {
                addToProfit += item.Balance * (precentage / 100);
            }
            );
            _profits += addToProfit;
        }
        internal void JoinAccounts(Account a1, Account a2)
        {
            // i did the same customer check in Account + operator
            Account a3 = a1 + a2;
            _accounts.Remove(a1);
            _mapAccountByAccountNmber.Remove(a1.AccountNumber);
            _accounts.Remove(a2);
            _mapAccountByAccountNmber.Remove(a2.AccountNumber);
        }

    }
}
