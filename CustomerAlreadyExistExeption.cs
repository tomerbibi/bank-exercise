﻿using System;
using System.Runtime.Serialization;

namespace ClassThing3
{
    [Serializable]
    internal class CustomerAlreadyExistExeption : Exception
    {
        public CustomerAlreadyExistExeption()
        {
        }

        public CustomerAlreadyExistExeption(string message) : base(message)
        {
        }

        public CustomerAlreadyExistExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CustomerAlreadyExistExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}