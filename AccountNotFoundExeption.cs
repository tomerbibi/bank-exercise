﻿using System;
using System.Runtime.Serialization;

namespace ClassThing3
{
    [Serializable]
    internal class AccountNotFoundExeption : Exception
    {
        public AccountNotFoundExeption()
        {
        }

        public AccountNotFoundExeption(string message) : base(message)
        {
        }

        public AccountNotFoundExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AccountNotFoundExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}